# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from .models import AdvantagesModel, AdvantagesItem


class AdvantagesAdminInline(admin.TabularInline):
    model = AdvantagesItem
    extra = 0


class AdvantagesPlugin(CMSPluginBase):
    model = AdvantagesModel
    name = u"Преимущества работы"
    inlines = [AdvantagesAdminInline]
    render_template = '_blocks/b-advantages/b-advantages.pug'


plugin_pool.register_plugin(AdvantagesPlugin)
