# coding: utf-8
from django.db import models
from cms.models import CMSPlugin


class AdvantagesModel(CMSPlugin):

    def copy_relations(self, oldinstance):
        self.advantagesitem_set.all().delete()

        for item in oldinstance.advantagesitem_set.all():
            item.pk = None
            item.advantages = self
            item.save()

    # def __unicode__(self):
    #     return self.title

    class Meta:
        verbose_name = u'Преимущества работы'
        verbose_name_plural = u'Преимущество работы'


class AdvantagesItem(models.Model):
    advantages = models.ForeignKey("AdvantagesModel", verbose_name=u"Преимущества")
    title = models.TextField(u"Преимущество", null=False, blank=False)
    image = models.ImageField(upload_to="advantages/%Y/")
