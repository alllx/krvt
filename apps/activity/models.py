# coding: utf-8
from django.db import models
from cms.models import CMSPlugin


class ActivityModel(CMSPlugin):

    def copy_relations(self, oldinstance):
        self.activityitem_set.all().delete()

        for item in oldinstance.activityitem_set.all():
            item.pk = None
            item.activity = self
            item.save()

    class Meta:
        verbose_name = u'История компании'
        verbose_name_plural = u'Историю компании'


class ActivityItem(models.Model):
    activity = models.ForeignKey(
        "ActivityModel", verbose_name=u"История компании ")
    title = models.CharField(u'Год', null=False, max_length=255)
    description = models.TextField(u"Описание", null=False, blank=False)
