# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from .models import ActivityModel, ActivityItem


class ActivityAdminInline(admin.TabularInline):
    model = ActivityItem
    extra = 0


class ActivityPlugin(CMSPluginBase):
    model = ActivityModel
    name = u"История компании"
    inlines = [ActivityAdminInline]
    render_template = '_blocks/b-feautures/b-feautures.pug'


plugin_pool.register_plugin(ActivityPlugin)
