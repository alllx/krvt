# coding: utf-8
from __future__ import unicode_literals
from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from .models import BlockPluginModel


class BlockPlugin(CMSPluginBase):
    model = BlockPluginModel
    name = u"Блок контентный"
    allow_children = True
    render_template = 'block/block.pug'


plugin_pool.register_plugin(BlockPlugin)
