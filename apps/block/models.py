# coding: utf-8
from __future__ import unicode_literals

from django.db import models
from cms.models import CMSPlugin
from colorfield.fields import ColorField


class BlockPluginModel(CMSPlugin):
    width = models.BooleanField(u"Ограничить ширину", default=True)
    css_add = models.CharField(
        u"CSS класс", max_length=255, null=True, blank=True)
    image = models.ImageField(
        u"Фоновое изображение", upload_to="blocks/%Y/", null=True, blank=True)
    color = ColorField(u"Изменить фоновый цвет", default='#ffffff')
    padding = models.CharField(
        u"Отступ", max_length=255, null=False, blank=False, default="20")
    light = models.BooleanField(u"Белый текст", default=False)

    class Meta:
        verbose_name = u"Блок"
        verbose_name_plural = u"Блок"
