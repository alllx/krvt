# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .models import Partner, PartnerPluginModel, Customer, Team, Project


class PartnersPlugin(CMSPluginBase):
    name = u"Партнёры"
    render_template = "cmsplugin_partners/plugin.pug"
    text_enabled = True
    cache = False
    model = PartnerPluginModel

    def render(self, context, instance, placeholder):
        context = super(PartnersPlugin, self).render(
            context, instance, placeholder)
        context["object_list"] = Partner.objects.filter(visible=True)
        return context


class CustomerPlugin(CMSPluginBase):
    name = u"Заказчики"
    render_template = "cmsplugin_partners/plugin-customers.pug"
    text_enabled = True
    cache = False
    model = PartnerPluginModel

    def render(self, context, instance, placeholder):
        context = super(CustomerPlugin, self).render(
            context, instance, placeholder)
        context["object_list"] = Customer.objects.filter(visible=True)
        return context


class TeamPlugin(CMSPluginBase):
    name = u"Команда"
    render_template = "cmsplugin_partners/plugin-team.pug"
    text_enabled = True
    cache = False
    model = PartnerPluginModel

    def render(self, context, instance, placeholder):
        context = super(TeamPlugin, self).render(
            context, instance, placeholder)
        context["object_list"] = Team.objects.filter(visible=True)
        return context


class ProjectPlugin(CMSPluginBase):
    name = u"Проекты"
    render_template = "cmsplugin_partners/plugin-projects.pug"
    text_enabled = True
    cache = False
    model = PartnerPluginModel

    def render(self, context, instance, placeholder):
        context = super(ProjectPlugin, self).render(
            context, instance, placeholder)
        context["object_list"] = Project.objects.filter(visible=True)
        return context


class CallToUsPlugin(CMSPluginBase):
    name = u"Связаться с нами"
    render_template = "cmsplugin_partners/plugin-call.pug"
    text_enabled = True
    cache = False
    model = PartnerPluginModel


class AboutPlugin(CMSPluginBase):
    name = u"О компании"
    render_template = "cmsplugin_partners/plugin-about.pug"
    text_enabled = True
    cache = False
    allow_children = True
    child_classes = [
        'TextPlugin'
    ]


plugin_pool.register_plugin(PartnersPlugin)
# plugin_pool.register_plugin(CustomerPlugin)
plugin_pool.register_plugin(TeamPlugin)
plugin_pool.register_plugin(ProjectPlugin)
# plugin_pool.register_plugin(CallToUsPlugin)
plugin_pool.register_plugin(AboutPlugin)
