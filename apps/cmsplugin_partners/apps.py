# coding: utf-8
from __future__ import unicode_literals
from django.apps import AppConfig


class CmsPluginPartnersConfig(AppConfig):
    name = "cmsplugin_partners"
    verbose_name = "Контент"
