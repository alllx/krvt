# coding: utf-8
from django.db import models

from cms.models import CMSPlugin
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer


class PartnerPluginModel(CMSPlugin):
    title = models.CharField(
        u"Заголовок", null=True, blank=True, max_length=255,
        default="")

    def __unicode__(self):
        return u"{0}".format(self.title or self.id)


class BasePartner(models.Model):
    title = models.CharField(
        u"Название", null=True, blank=True, max_length=255)
    image = models.ImageField(u"Изображение", upload_to="partner/%Y/%m/%d/")
    visible = models.BooleanField(u"Выводить", default=True)
    url = models.URLField("Ссылка", null=True, blank=True)
    cropping = ImageRatioField("image", "250x50")
    subtitle = models.CharField(
        u"Подзаголовок", null=True, blank=True, max_length=255)
    description = models.TextField(
        u"Описание", null=True, blank=True, max_length=255)
    linkurl = models.CharField(
        "Страница", null=True, blank=True, max_length=255)
    linktext = models.CharField(
        u"Текст кнопки", null=True, blank=True, max_length=255)

    def __unicode__(self):
        return u"{0}".format(self.title or self.id)

    def get_thumbnail_url(self):
        thumbnail_url = get_thumbnailer(self.image).get_thumbnail({
            "size": (250, 50), "box": self.cropping, "crop": True,
            "detail": True}).url
        return thumbnail_url

    class Meta:
        abstract = True


class Partner(BasePartner):
    class Meta:
        verbose_name = u"партнер"
        verbose_name_plural = u"партнеры"


class Customer(BasePartner):
    class Meta:
        verbose_name = u"заказчик"
        verbose_name_plural = u"заказчики"


class Team(BasePartner):
    class Meta:
        verbose_name = u"Участник команды"
        verbose_name_plural = u"участники команды"


class Project(BasePartner):
    class Meta:
        verbose_name = u"Проект"
        verbose_name_plural = u"проекты"


class CallToUs(BasePartner):

    class Meta:
        verbose_name = u"Форма"
        verbose_name_plural = u"формы"


class About(BasePartner):
    class Meta:
        verbose_name = u"Описания"
        verbose_name_plural = u"описание"
