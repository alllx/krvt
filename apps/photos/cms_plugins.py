# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import Photos, PhotosWithoutText


class PhotosPlugin(CMSPluginBase):
    name = u"Слайдер фото"
    render_template = '_blocks/b-promo-slider/b-promo-slider.pug'
    allow_children = True
    child_classes = [
        'PhotosSinglePlugin',
        'PhotosHotelSingle'
    ]


class PhotosHotelPlugin(CMSPluginBase):
    name = u"Слайдер отеля"
    render_template = '_blocks/b-promo-slider/b-promo-slider-single.pug'
    allow_children = True
    child_classes = [
        'PhotosHotelSingle',
    ]


class PhotosSinglePlugin(CMSPluginBase):
    model = Photos
    name = u"Слайд с текстом"
    render_template = '_blocks/b-promo-slider/b-promo-slider-item.pug'


class PhotosHotelSingle(CMSPluginBase):
    model = PhotosWithoutText
    name = u"Слайд без текста"
    render_template = '_blocks/b-promo-slider/b-promo-slider-item.pug'


plugin_pool.register_plugin(PhotosPlugin)
plugin_pool.register_plugin(PhotosSinglePlugin)
plugin_pool.register_plugin(PhotosHotelPlugin)
plugin_pool.register_plugin(PhotosHotelSingle)
