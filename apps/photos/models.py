# coding: utf-8
from django.db import models
from cms.models import CMSPlugin


class Photos(CMSPlugin):
    image = models.ImageField(u"Изображение", upload_to="carousel/%Y/")
    title = models.CharField(
        u"Заголовок", max_length=255, null=True, blank=True)
    description = models.TextField(u"Описание", null=True, blank=True)
    link = models.CharField(u"Ссылка", max_length=255, null=True, blank=True)


class PhotosWithoutText(CMSPlugin):
    image = models.ImageField(u"Изображение", upload_to="carousel/%Y/")
