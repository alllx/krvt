# coding: utf-8
from django.db import models
from cms.models import CMSPlugin


class PartnersSliderModel(CMSPlugin):

    def copy_relations(self, oldinstance):
        self.partnersslideritem_set.all().delete()

        for item in oldinstance.partnersslideritem_set.all():
            item.pk = None
            item.slide = self
            item.save()

    class Meta:
        verbose_name = u'Партнеры'
        verbose_name_plural = u'Партнер'


class PartnersSliderItem(models.Model):
    partner = models.ForeignKey("PartnersSliderModel", verbose_name=u"Партнер")
    image = models.ImageField(upload_to="partners_slider/%Y/")
