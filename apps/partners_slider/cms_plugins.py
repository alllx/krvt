# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.contrib import admin
from .models import PartnersSliderModel, PartnersSliderItem


class PartnersSliderAdminInline(admin.TabularInline):
    model = PartnersSliderItem
    extra = 0


class partnersSliderPlugin(CMSPluginBase):
    model = PartnersSliderModel
    name = u"Партнерский слайдер"
    inlines = [PartnersSliderAdminInline]
    render_template = "_blocks/b-photo/b-photo.jade"

    def render(self, context, instance, placeholder):
        context["object_list"] = instance.partnersslideritem_set.all()
        context["title"] = u"Партнёры"
        return context


plugin_pool.register_plugin(partnersSliderPlugin)
