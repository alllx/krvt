# coding: utf-8
from django.db import models
from cms.models import CMSPlugin


class PhotoCarouselItem(CMSPlugin):
    image = models.ImageField(upload_to="photo_carousel/%Y/")
    title = models.CharField("Заголовок", null=False,
                             blank=False, max_length=255)
    description = models.CharField(
        "Описание", null=False, blank=False, max_length=255)

    class Meta:
        verbose_name = u'Слайды'
        verbose_name_plural = u'Слайд'
