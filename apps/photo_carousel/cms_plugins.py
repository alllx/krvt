# coding: utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from .models import PhotoCarouselItem


class PhotoCarouselPlugin(CMSPluginBase):
    name = u"Фотогалерея"
    render_template = "_blocks/b-photo/b-photo.pug"
    allow_children = True
    child_classes = [
        'PhotoCarouselSlidePlugin'
    ]


class PhotoAllPlugin(CMSPluginBase):
    name = u"Фотогалерея все фото"
    render_template = "_blocks/b-photo/b-photo-all.pug"

    def render(self, context, instance, placeholder):
        context = super(PhotoAllPlugin, self).render(
            context, instance, placeholder)
        context["object_list"] = PhotoCarouselItem.objects.all()
        return context


class PhotoCarouselSlidePlugin(CMSPluginBase):
    model = PhotoCarouselItem
    name = u"Слайд фотогалереи"
    render_template = "_blocks/b-photo/b-photo-item.pug"


plugin_pool.register_plugin(PhotoCarouselPlugin)
plugin_pool.register_plugin(PhotoCarouselSlidePlugin)
plugin_pool.register_plugin(PhotoAllPlugin)
