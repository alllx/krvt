# coding: utf-8
from __future__ import unicode_literals
from django.apps import AppConfig


class DefaultSettingsConfig(AppConfig):
    name = "default_settings"
    verbose_name = "Настройки по-умолчанию"
