from default_settings.models import DefaultBackgroundSetting
from django import template

register = template.Library()


@register.assignment_tag
def default_background():
    try:
        return DefaultBackgroundSetting.objects.all()[0].image.url
    except IndexError:
        return ''
