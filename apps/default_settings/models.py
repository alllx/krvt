# coding: utf-8
from django.db import models


class DefaultBackgroundSetting(models.Model):
    image = models.ImageField(
        u"Фоновое изображение страницы", null=True, blank=True,
        upload_to="uploads/")

    class Meta:
        verbose_name = u'Фоновое изображение страницы'
        verbose_name_plural = u'Фоновое изображение страницы'
