# coding: utf-8
from django.contrib import admin
from .models import DefaultBackgroundSetting

admin.site.register(DefaultBackgroundSetting)
