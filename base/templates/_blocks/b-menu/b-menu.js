var $menuDropdown = $("[menu-list]");

$("[menu-toggle]").mouseout(function () {
    $menuDropdown.addClass("active");
});

$(".dropdown-toggle").on("click", function (e) {
    e.preventDefault();
    var link = $(this).attr("href");
    location.href = link;
});

if (window.innerWidth > 960) {
    $(document).mouseup(function (e) {
        if (!$menuDropdown.is(e.target)
            && $menuDropdown.has(e.target).length === 0) {
            $menuDropdown.removeClass("active");
        }
    });
}

$(".b-menu__toggle").on("click", function () {
    $(this).toggleClass("active");
    $(".b-menu__list").toggleClass("active");
    $("body").toggleClass("modal-open");
    $(".b-header__action").toggleClass("fixed");
});

$(".b-menu__item").on("click", function () {
    $(".b-menu__list").removeClass("active");
    $("body").removeClass("modal-open");
    $(".b-header__action").removeClass("fixed");
});


$(function () {
    var screenSize;
    if (window.innerWidth > 960) {
        screenSize = 300;

        var fixedHeader = screenSize;
        $(window).scroll(function () {
            var scroll = scrollTop();
            if (scroll >= fixedHeader) {
                $(".b-header").addClass("b-header--small");
                $(".b-menu__toggle").addClass("fixed");
            }
            else {
                $(".b-header").removeClass("b-header--small");
                $(".b-menu__toggle").removeClass("fixed");
            }
        });
    }
    function scrollTop() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
});
