(function($) {
    "use strict";
    $(document).ready(function() {
        $("[data-call-modal]").on("click", function() {
            $("#callModal").modal("show");
        });
        $(document).on("submit", "#callModalform", function(e) {
            e.preventDefault();
            $.ajax($(this).data("request-url"), {
                type: "GET",
                contentType: "application/json",
                data: {
                    name: $("#callModalName").val(),
                    phone: $("#callModalPhone").val(),
                },
                dataType: "json",
                cache: false,
                success: function(response) {
                    if (response.result === "success") {
                        $("#notifyMessage").html(response.message);
                        $("#notifyModal").modal();
                        $("#callModalName").val("");
                        $("#callModalPhone").val("");

                    } else {
                        $("#notifyModal").modal();
                        $("#notifyMessage").html("Сообщение не было отправлено");
                    }
                }
            });
        });

    });
})(window.jQuery);