// setting in base/static/js/swiper.js:1236

/*eslint-disable */
window.swiper = function () {
    new Swiper(".swiper-list", {
        slidesPerView: 2,
        spaceBetween: 0,
        keyboardControl: true,
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        centeredSlides: true,
        loop: true,
        breakpoints: {
            1024: {
                slidesPerView: 2,
                centeredSlides: true
            },
            768: {
                slidesPerView: 1,
                centeredSlides: false,
            }
        }
    });
    new Swiper(".swiper-single", {
        slidesPerView: 1,
        spaceBetween: 0,
        keyboardControl: true,
        nextButton: ".swiper-button-next",
        prevButton: ".swiper-button-prev",
        centeredSlides: false,
        loop: true,
    });
}();
/*eslint-enable */
