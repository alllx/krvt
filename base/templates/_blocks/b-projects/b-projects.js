/*eslint-disable */
$("[data-project-carousel]").slick({
    arrows: true,
    infinite: true,
    slidesToShow: 3,
    speed: 10,
    responsive: [{
        breakpoint: 959,
        settings: {
            slidesToShow: 2,
            arrows: false
        }
    },
    {
        breakpoint: 479,
        settings: {
            slidesToShow: 1,
            // arrows: false
        }
    }
    ]
});
/*eslint-enable */
