$("#photo-carousel")
    .on("afterChange", function (event, slick, currentSlide) {
        $("#photo-carousel-second").slick("slickGoTo", currentSlide - 2);
        $("#photo-carousel-third").slick("slickGoTo", currentSlide - 1);
    }).slick({
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        speed: 10,
        dots: false,
    });

$("#photo-carousel-second").slick({
    arrows: false,
    infinite: true,
    slidesToShow: 2,
    speed: 10,
    dots: false,
    touchMove: false,
    touchThreshold: false,
    swipe: false,
    swipeToSlide: false,
    initialSlide: 1
});

$("#photo-carousel-third").slick({
    arrows: false,
    infinite: true,
    slidesToShow: 3,
    speed: 10,
    dots: false,
    touchMove: false,
    touchThreshold: false,
    swipe: false,
    swipeToSlide: false,
    initialSlide: 2
});
