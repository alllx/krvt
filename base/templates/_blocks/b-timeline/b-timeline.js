$("[data-timeline-year-link]").on("click", function (e) {
    e.preventDefault();
});

$("[data-timeline-year-link]:not(.active)").on("click", function () {
    var year = $(this).data("timeline-year-link");
    $("[data-timeline-year-link]").removeClass("active");
    $(this).addClass("active");
    $("[data-timeline-year]").hide();
    $("[data-timeline__single-year]").html(year+" г");
    $("[data-timeline-year=" + year + "]").show();
});

$("[data-timeline-year-link]").first().click();
