/*eslint-disable */
$("[data-partners-carousel]").slick({
    slidesToShow: 4,
    arrows: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
        breakpoint: 960,
        settings: {
            slidesToShow: 2
        }
    },
    {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
            autoplay: false
        }
    },
    {
        breakpoint: 330,
        settings: {
            slidesToShow: 1,
            autoplay: false
        }
    }
    ]
});
/*eslint-enable */
