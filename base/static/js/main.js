/*eslint-disable */
if (module.hot) {
    module.hot.accept();
}

"use strict";

import "../stylesheets/main.less";

import "./tools/get_cookie";
import "./tools/csrf";
import "./tools/sizzle";
import "./tools/object-fit-images";
import "./tools/fancybox";

import "slick-carousel";
import "slick-carousel/slick/slick-theme.less";
import "b-promo-slider/slick.less";
import "b-project/fancybox.css";
import "bootstrap";
import "b-photo/lightbox.css";
import "b-photo/lightbox.js";
import "b-call-modal/b-call-modal.js";




import "bootstrap";
import "b-promo-slider/swiper.less";
import "b-promo-slider/swiper.js";
import "b-promo-slider/b-promo-slider.js";
import "b-menu/b-menu.js";
import "b-feautures/b-feautures.js";
import "b-history/b-history.js";
import "b-photo/b-photo.js";
import "b-timeline/b-timeline.js";
import "b-partners/b-partners.js";
import "b-projects/b-projects.js";
import "b-rating/b-rating.js";
