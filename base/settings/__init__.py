""" Settings for base """

from .base import *  # noqa
try:
    from .local import *  # noqa
except ImportError, exc:  # noqa
    exc.args = tuple(
        ['%s (did you rename settings/local-dist.py?)' % exc.args[0]])
    raise exc
