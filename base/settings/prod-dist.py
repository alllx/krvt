"""
This is an example settings/local.py file.
These settings overrides what's in settings/base.py
"""

from . import base


# To extend any settings from settings/base.py here's an example.
# If you don't need to extend any settings from base.py, you do not need
# to import base above
INSTALLED_APPS = base.INSTALLED_APPS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'base_db',
        'USER': 'base',
        'PASSWORD': 'secret',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
    ('Alexey Pudnikov', 'a@welcomemedia.ru'),
)
MANAGERS = ADMINS

# SECURITY WARNING: don't run with debug turned on in production!
# Debugging displays nice error messages, but leaks memory. Set this to False
# on all server instances and True only for development.
DEBUG = DEV = True
ALLOWED_HOSTS = ["*"]  # noqa
DEBUG = DEV = False
ALLOWED_HOSTS = ["base.2-wm.ru"]  # noqa

# Is this a development instance? Set this to True on development/master
# instances and False on stage/prod.

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts

# SECURITY WARNING: keep the secret key used in production secret!
# Hardcoded values can leak through source control. Consider loading
# the secret key from an environment variable or a file instead.
SECRET_KEY = 'lrpg$_uyy*l7-z8hshmq3rus^9jt1z4@e_d*v8r1*x9$am!$+k'

# Log settings
DEBUG_TOOLBAR_PATCH_SETTINGS = False
