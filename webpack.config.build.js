var path = require("path");
var webpack = require("webpack");
var CleanWebpackPlugin = require("clean-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpackConfig = require("./webpack.config");
var BundleTracker = require('webpack-bundle-tracker');

var cssLoader = "css-loader";

webpackConfig.devtool = false;

webpackConfig.plugins.push(
    new CleanWebpackPlugin(["base/static/build"]),
    new ExtractTextPlugin("[name].[hash].css"),
    new BundleTracker({ filename: './webpack-stats-build.json' })
);

webpackConfig.module.rules.push({
    test: /\.css$/,
    loader: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: [cssLoader, "postcss-loader"]
    }),

    test: /\.less$/,
    loader: ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: [cssLoader, "postcss-loader", "less-loader"]
    })
});

webpackConfig.output = {
    path: path.join(__dirname, "base/static/build"),
    filename: "[name].[hash].js"
};

module.exports = webpackConfig;
