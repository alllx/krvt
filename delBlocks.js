"use strict";

const
    fs = require("fs"),
    del = require("del"),
    blocks = "base/templates/_blocks",
    templates = "base/templates/_blocksList.pug",
    components = "base/static/stylesheets/components.less",
    typography = "- include '_typography/typography.pug'";

del.sync([blocks + "/**", "!" + blocks]);

/* eslint-disable no-console */
fs.writeFile(components, "", (err) => {
    if (err) throw err;
    console.log("styles and imports removed");
});

fs.writeFile(templates, typography, (err) => {
    if (err) throw err;
    console.log("components removed");
});
/* eslint-disable no-console */


