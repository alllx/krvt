var path = require('path');
var webpackConfig = require('./webpack.config');
// var DashboardPlugin = require('webpack-dashboard/plugin');

var cssLoader = 'css-loader?sourceMap=inline?importLoaders=1';

webpackConfig.devtool = 'source-map';

webpackConfig.output = {
    pathinfo: true,
    publicPath: 'http://localhost:3000/build/',
    path: path.resolve(__dirname, 'build'),
    filename: '[name].[hash].js'
};

// webpackConfig.plugins.push(
//   new DashboardPlugin()
// );

webpackConfig.module.rules.push({
    test: /\.less$/,
    use: [
        'style-loader',
        cssLoader,
        'postcss-loader?sourceMap=inline',
        'less-loader?sourceMap=inline'
    ]
});

webpackConfig.devServer = {
    contentBase: path.join(__dirname, "build"),
    compress: true,
    port: 3000,
    proxy: {
        "/": {
            target: "http://localhost:8000",
            secure: false,
            publicPath: "http://localhost:3000/build/"
        }
    },
}

module.exports = webpackConfig;
