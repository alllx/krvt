'use strict';

const
    fs = require('fs'),
    path = require('path'),
    readline = require('readline'),
    createInterface = readline.createInterface;

const rl = createInterface(process.stdin, process.stdout);

const BLOCKS_DIR = path.join(__dirname, 'base/templates/_blocks');

const fileSources = {
    pug: `.b-{blockName}\n\t.b-{blockName}__inner\n\t\t.b-{blockName}__title b-{blockName}`,
    less: `.b-{blockName} {\n\t \n\t&__inner {\n\t\t.inner();\n\t}\n\n\t&__title {\n\t\t.title();\n\t}\n}`
};

function validateBlockName(blockName) {
    return new Promise((resolve, reject) => {
        const isValid = /^(\d|\w|-)+$/.test(blockName);

        if (isValid) {
            resolve(isValid);
        } else {
            const errMsg = (
                `ERR>>> An incorrect block name b-'${blockName}'\n` +
                `ERR>>> A block name must include letters, numbers & the minus symbol.`
            );
            reject(errMsg);
        }
    });
}

function directoryExist(blockPath, blockName) {
    return new Promise((resolve, reject) => {
        fs.stat(blockPath, notExist => {
            if (notExist) {
                resolve();
            } else {
                reject(`ERR>>> The block b-'${blockName}' already exists.`);
            }
        });
    });
}

function createDir(dirPath) {
    return new Promise((resolve, reject) => {
        fs.mkdir(dirPath, err => {
            if (err) {
                reject(`ERR>>> Failed to create a folder b-'${dirPath}'`);
            } else {
                resolve();
            }
        });
    });
}

function createFiles(blocksPath, blockName) {
    const promises = [];
    Object.keys(fileSources).forEach(ext => {
        const fileSource = fileSources[ext].replace(/\{blockName}/g, blockName);
        const filename = `b-${blockName}.${ext}`;
        const filePath = path.join(blocksPath, filename);

        promises.push(
            new Promise((resolve, reject) => {
                fs.writeFile(filePath, fileSource, 'utf8', err => {
                    if (err) {
                        reject(`ERR>>> Failed to create a file '${filePath}'`);
                    } else {
                        resolve();
                    }
                });
            })
        );
    });

    return Promise.all(promises);
}

function getFiles(blockPath) {
    return new Promise((resolve, reject) => {
        fs.readdir(blockPath, (err, files) => {
            if (err) {
                reject(`ERR>>> Failed to get a file list from a folder '${blockPath}'`);
            } else {
                resolve(files);
            }
        });
    });
}

function pugConnect(blockName) {
    return new Promise((resolve, reject) => {
        fs.appendFile(`base/templates/_blocksList.pug`, `\n//- block b-${blockName} \n- include '_blocks/b-${blockName}/b-${blockName}.pug' \n`, err => {
            if (err) {
                reject(`ERR>>> Failed to rewrite base/templates/_blocksList.pug`);
            } else {
                resolve();
            }
        });
    });
}

function lessConnect(blockName) {
    return new Promise((resolve, reject) => {
        fs.appendFile(`base/static/stylesheets/components.less`, `\n@import '../../templates/_blocks/b-${blockName}/b-${blockName}.less';`, err => {
            if (err) {
                reject(`ERR>>> Failed to rewrite stylesheets/style.less`);
            } else {
                resolve();
            }
        });
    });
}

function printErrorMessage(errText) {
    console.log(errText);
    rl.close();
}

function initMakeBlock(candidateBlockName) {
    const blockNames = candidateBlockName.trim().split(/\s+/);

    const makeBlock = blockName => {
        const blockPath = path.join(BLOCKS_DIR, 'b-' + blockName);

        return validateBlockName(blockName)
            .then(() => directoryExist(blockPath, blockName))
            .then(() => createDir(blockPath))
            .then(() => createFiles(blockPath, blockName))
            .then(() => getFiles(blockPath))
            .then(files => {
                const line = '-'.repeat(48 + blockName.length);
                console.log(line);
                console.log('');
                console.log(`Компонент b-${blockName} создан`);
                console.log('');
                console.log(`Подключаем:`);
                console.log(`- include '_blocks/b-${blockName}/b-${blockName}.pug'`);
                console.log('');
                console.log(line);

                files.forEach(file => console.log(file));

                rl.close();
            })
            .then(() => pugConnect(blockName))
            .then(() => lessConnect(blockName));
    };

    if (blockNames.length === 1) {
        return makeBlock(blockNames[0]);
    }

    const promises = blockNames.map(name => makeBlock(name));
    return Promise.all(promises);
}

const blockNameFromCli = process.argv
    .slice(2)
    .join(' ');

if (blockNameFromCli !== '') {
    initMakeBlock(blockNameFromCli).catch(printErrorMessage);
} else {
    rl.setPrompt('Block(s) name: ');
    rl.prompt();
    rl.on('line', (line) => {
        initMakeBlock(line).catch(printErrorMessage);
    });
}
