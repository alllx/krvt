# KravtInvest
Кравт отель в Петербурге

## О прокте
http://kravt.2-wm.ru

## Зависимости

- Python 2.7
- pip
- [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)
- [Django 1.9](https://docs.djangoproject.com/en/1.9/)
- [Django CMS 3.4.x](http://docs.django-cms.org/en/release-3.4.x/)
- [pypugjs](https://github.com/matannoam/pypugjs)

## Установка
Создаем виртуальное окружение

`mkvirtualenv KravtInvest`

Устанавливаем серверные библиотеки

`pip install -r requirements.txt`

Фронтенд )

`npm i`

Создаем файл локальных настроек из шаблона

`cp base/settings/local-dist.py base/settings/local.py`

Запускаем миграции

`python manage.py migrate`

Создаем суперпользователя

`python manage.py createsuperuser`

## Использование

Запускаем сервер

`python manage.py runserver`

Собираем билд

`npm run build`

Запускаем одновременно сервер и сборщика

`npm run start`

## Структура проекта
- **apps**
Папка с приложениями для Django. В каждом приложении могут быть свои шаблоны и статика относящиеся только к нему
- **conf**
Серверные конфигурационные файлы
- **base** папка с шаблонами, статикой и настройками сервера
- **requirements** папка с серверными библиотеками разбитыми по файлам
